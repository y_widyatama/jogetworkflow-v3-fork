package telkom.isc;

import org.joget.directory.model.Department;
import org.joget.directory.model.Employment;
import org.joget.directory.model.EmploymentReportTo;
import org.joget.directory.model.Grade;
import org.joget.directory.model.Group;
import org.joget.directory.model.Organization;
import org.joget.directory.model.Role;
import org.joget.directory.model.User;
import org.joget.directory.model.service.DirectoryManager;
import org.joget.directory.model.service.DirectoryManagerImpl;
import org.joget.directory.model.service.ExtDirectoryManager;

import java.util.ArrayList;
import java.util.Collection;
import org.joget.commons.util.StringUtil;
import org.joget.directory.dao.DepartmentDao;
import org.joget.directory.dao.EmploymentDao;
import org.joget.directory.dao.GradeDao;
import org.joget.directory.dao.GroupDao;
import org.joget.directory.dao.OrganizationDao;
import org.joget.directory.dao.RoleDao;
import org.joget.directory.dao.UserDao;

public class TDirectoryManager implements DirectoryManager,ExtDirectoryManager 
{
	Activator activator;
	ExtDirectoryManager wrapped;
	protected TDirectoryManager(ExtDirectoryManager w,Activator a) {
		wrapped = w;
		activator=a;
	}
	public boolean authenticate(String username, String password) {
		if (activator.tryLdap(username,password)) return true;
		return wrapped.authenticate(username, password);
	}
	public Department getDepartmentById(String departmentId) {
		return wrapped.getDepartmentById(departmentId);
	}
	public Department getDepartmentByName(String name) {
		return wrapped.getDepartmentByName(name);
	}
	public User getDepartmentHod(String departmentId) {
		return wrapped.getDepartmentHod(departmentId);
	}
	public Collection<Department> getDepartmentList() {
		return wrapped.getDepartmentList();
	}
	public Collection<Department> getDepartmentList(String sort, Boolean desc,
			Integer start, Integer rows) {
		return wrapped.getDepartmentList(sort, desc, start, rows);
	}
	public Collection<Department> getDepartmentListByOrganization(
			String organizationId, String sort, Boolean desc, Integer start,
			Integer rows) {
		return wrapped.getDepartmentListByOrganization(organizationId, sort,
				desc, start, rows);
	}
	public Collection<User> getDepartmentUserByGradeId(String departmentId,
			String gradeId) {
		return wrapped.getDepartmentUserByGradeId(departmentId, gradeId);
	}
	public Collection<Department> getDepartmentsByOrganizationId(
			String filterString, String organizationId, String sort,
			Boolean desc, Integer start, Integer rows) {
		return wrapped.getDepartmentsByOrganizationId(filterString,
				organizationId, sort, desc, start, rows);
	}
	public Collection<Department> getDepartmentsByParentId(String filterString,
			String parentId, String sort, Boolean desc, Integer start,
			Integer rows) {
		return wrapped.getDepartmentsByParentId(filterString, parentId, sort,
				desc, start, rows);
	}
	public Employment getEmployment(String id) {
		return wrapped.getEmployment(id);
	}
	public Collection<Employment> getEmployments(String filterString,
			String organizationId, String departmentId, String gradeId,
			String sort, Boolean desc, Integer start, Integer rows) {
		return wrapped.getEmployments(filterString, organizationId,
				departmentId, gradeId, sort, desc, start, rows);
	}
	public Grade getGradeById(String gradeId) {
		return wrapped.getGradeById(gradeId);
	}
	public Grade getGradeByName(String name) {
		return wrapped.getGradeByName(name);
	}
	public Collection<Grade> getGradeList() {
		return wrapped.getGradeList();
	}
	public Collection<Grade> getGradesByOrganizationId(String filterString,
			String organizationId, String sort, Boolean desc, Integer start,
			Integer rows) {
		return wrapped.getGradesByOrganizationId(filterString, organizationId,
				sort, desc, start, rows);
	}
	public Group getGroupById(String groupId) {
		return wrapped.getGroupById(groupId);
	}
	public Group getGroupByName(String groupName) {
		return wrapped.getGroupByName(groupName);
	}
	public Collection<Group> getGroupByUsername(String username) {
		return wrapped.getGroupByUsername(username);
	}
	public Collection<Group> getGroupList() {
		return wrapped.getGroupList();
	}
	public Collection<Group> getGroupList(String filterString, String sort,
			Boolean desc, Integer start, Integer rows) {
		return wrapped.getGroupList(filterString, sort, desc, start, rows);
	}
	public Collection<Group> getGroupsByOrganizationId(String filterString,
			String organizationId, String sort, Boolean desc, Integer start,
			Integer rows) {
		return wrapped.getGroupsByOrganizationId(filterString, organizationId,
				sort, desc, start, rows);
	}
	public Collection<Group> getGroupsByUserId(String filterString,
			String userId, String organizationId, Boolean inGroup, String sort,
			Boolean desc, Integer start, Integer rows) {
		return wrapped.getGroupsByUserId(filterString, userId, organizationId,
				inGroup, sort, desc, start, rows);
	}
	public Organization getOrganization(String id) {
		return wrapped.getOrganization(id);
	}
	public Organization getOrganizationByName(String name) {
		return wrapped.getOrganizationByName(name);
	}
	public Collection<Organization> getOrganizationsByFilter(
			String filterString, String sort, Boolean desc, Integer start,
			Integer rows) {
		return wrapped.getOrganizationsByFilter(filterString, sort, desc,
				start, rows);
	}
	public Department getParentDepartment(String id) {
		return wrapped.getParentDepartment(id);
	}
	public Department getParentDepartmentByName(String name) {
		return wrapped.getParentDepartmentByName(name);
	}
	
	public Long getTotalDepartmentnsByOrganizationId(String filterString,
			String organizationId) {
		return wrapped.getTotalDepartmentnsByOrganizationId(filterString,
				organizationId);
	}
	public Long getTotalDepartments(String organizationId) {
		return wrapped.getTotalDepartments(organizationId);
	}
	public Long getTotalDepartmentsByParentId(String filterString,
			String parentId) {
		return wrapped.getTotalDepartmentsByParentId(filterString, parentId);
	}
	public Long getTotalEmployments(String filterString, String organizationId,
			String departmentId, String gradeId) {
		return wrapped.getTotalEmployments(filterString, organizationId,
				departmentId, gradeId);
	}
	public Long getTotalGradesByOrganizationId(String filterString,
			String organizationId) {
		return wrapped.getTotalGradesByOrganizationId(filterString,
				organizationId);
	}
	public Long getTotalGroups() {
		return wrapped.getTotalGroups();
	}
	public Long getTotalGroupsByOrganizationId(String filterString,
			String organizationId) {
		return wrapped.getTotalGroupsByOrganizationId(filterString,
				organizationId);
	}
	public Long getTotalGroupsByUserId(String filterString, String userId,
			String organizationId, Boolean inGroup) {
		return wrapped.getTotalGroupsByUserId(filterString, userId,
				organizationId, inGroup);
	}
	public Long getTotalOrganizationsByFilter(String filterString) {
		return wrapped.getTotalOrganizationsByFilter(filterString);
	}
	
	public Long getTotalUsers() {
		return wrapped.getTotalUsers();
	}
	public Long getTotalUsers(String filterString, String organizationId,
			String departmentId, String gardeId, String groupId, String roleId,
			String active) {
		return wrapped.getTotalUsers(filterString, organizationId,
				departmentId, gardeId, groupId, roleId, active);
	}
	public Long getTotalUsersSubordinate(String username) {
		return wrapped.getTotalUsersSubordinate(username);
	}
	public Collection<User> getUserByDepartmentId(String departmentId) {
		return wrapped.getUserByDepartmentId(departmentId);
	}
	public Collection<User> getUserByGradeId(String gradeId) {
		return wrapped.getUserByGradeId(gradeId);
	}
	public Collection<User> getUserByGroupId(String groupId) {
		return wrapped.getUserByGroupId(groupId);
	}
	public Collection<User> getUserByGroupName(String groupName) {
		return wrapped.getUserByGroupName(groupName);
	}
	public User getUserById(String userId) {
		return wrapped.getUserById(userId);
	}
	public Collection<User> getUserByOrganizationId(String organizationId) {
		return wrapped.getUserByOrganizationId(organizationId);
	}
	public User getUserByUsername(String username) {
		return wrapped.getUserByUsername(username);
	}
	
	public Collection<User> getUserDepartmentUser(String username) {
		return wrapped.getUserDepartmentUser(username);
	}
	public Collection<User> getUserHod(String username) {
		return wrapped.getUserHod(username);
	}
	public Collection<User> getUserList() {
		return wrapped.getUserList();
	}
	public Collection<User> getUserList(String filterString, String sort,
			Boolean desc, Integer start, Integer rows) {
		return wrapped.getUserList(filterString, sort, desc, start, rows);
	}
	public Collection<Role> getUserRoles(String username) {
		return wrapped.getUserRoles(username);
	}
	public Collection<User> getUserSubordinate(String username) {
		return wrapped.getUserSubordinate(username);
	}
	public Collection<User> getUsers(String filterString,
			String organizationId, String departmentId, String gardeId,
			String groupId, String roleId, String active, String sort,
			Boolean desc, Integer start, Integer rows) {
		return wrapped.getUsers(filterString, organizationId, departmentId,
				gardeId, groupId, roleId, active, sort, desc, start, rows);
	}
	public Collection<User> getUsersSubordinate(String username, String sort,
			Boolean desc, Integer start, Integer rows) {
		return wrapped.getUsersSubordinate(username, sort, desc, start, rows);
	}
	public boolean isUserInGroup(String username, String groupName) {
		return wrapped.isUserInGroup(username, groupName);
	}
	
	
}

package telkom.isc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;

import org.joget.directory.model.service.DirectoryManager;
import org.joget.directory.model.service.DirectoryManagerPlugin;
import org.joget.directory.model.service.ExtDirectoryManager;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginProperty;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

import javax.naming.directory.InitialDirContext;
import javax.naming.directory.DirContext;
import javax.naming.Context;
import javax.naming.NamingException;


public class Activator extends DefaultPlugin implements DirectoryManagerPlugin {
	@Override public void start(BundleContext context) {
		super.start(context);
		try {
			ServiceReference[] references = context.getAllServiceReferences(null, null);
			for (ServiceReference sr : references)
			{
				String[] propertyKeys = sr.getPropertyKeys();
				System.err.println("bundle " +sr.getBundle().getSymbolicName());
				for (String key : propertyKeys)
				{
					System.err.print(key+":type"+
							sr.getProperty(key).getClass().getName()+"value="+sr.getProperty(key).toString()
							+ " ");
				}
				System.err.println();
			}
			
		} catch (InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	public Object execute(Map arg0) {
		System.err.println("Activator!execute");
		for (Object key : arg0.keySet())
		{
			String type = "?";
			type = (arg0.get(key)).getClass().getCanonicalName();
			if (type==null) type = "<?>";
			System.err.println("key "+key.toString()+" type "+type);
		}
		return null;
	}

	public String getDescription() {
		return "Telkom LDAP Plugin 2";
	}

	public String getName() {
		// TODO Auto-generated method stub
		return "TelkomLDAP";
	}

	public PluginProperty[] getPluginProperties() {
		PluginProperty[] properties = new PluginProperty[] { 
			        new PluginProperty("ldaphost", "LDAP Host", PluginProperty.TYPE_TEXTFIELD, null, "ldapnas1.telkom.co.id"), 
			        new PluginProperty("ldapport", "LDAP Port", PluginProperty.TYPE_TEXTFIELD, null, "389")
			    };
		return properties;
	}

	public String getVersion() {
		// TODO Auto-generated method stub
		return "1.0.0";
	}
	String ldapAddr = "127.0.0.1";
	String ldapPort = "389";
	boolean tryLdap(String username, String pass)
	{
		//String ENTRYDN = "cn="+"abc"+", ou=usermast,o="+"abc.com"+",c=US";// This is rootDN
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,"ldap://"+ldapAddr+":"+ldapPort);

		try{

		env.put(Context.SECURITY_PRINCIPAL,username);//DN
		env.put(Context.SECURITY_CREDENTIALS,pass);//This is password

		DirContext ctx = new InitialDirContext(env);
		
		return true;
		}
		catch (NamingException nx)
		{
			return false;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return false;
	}
	public DirectoryManager getDirectoryManagerImpl(Map arg0) {
		//System.err.println("Activator!getDirectoryManagerImpl");
		ExtDirectoryManager originalManager=null;
		for (Object key : arg0.keySet())
		{
			String type = "?";
			Object o= arg0.get(key);
			type = o.getClass().getCanonicalName();
			if (type==null) type = "<?>";
			if (key.equals("ldaphost") && o!=null) this.ldapAddr = o.toString(); 
			if (key.equals("ldapport") && o!=null) this.ldapPort = o.toString();
			if (o instanceof ExtDirectoryManager)
			{
				originalManager = (ExtDirectoryManager) o;
			}
			//System.err.println("key "+key.toString()+" type "+type);
		}
		
		if (originalManager!=null) System.err.println("Got original dir manager!");
		// TODO Auto-generated method stub
		return new TDirectoryManager(originalManager,this);
	}
/*
    protected Collection<ServiceRegistration> registrationList;

    public void start(BundleContext context) {
        registrationList = new ArrayList<ServiceRegistration>();

        //Register plugin here
        //registrationList.add(context.registerService(MyPlugin.class.getName(), new MyPlugin(), null));
    }

    public void stop(BundleContext context) {
        for (ServiceRegistration registration : registrationList) {
            registration.unregister();
        }
    }*/
}
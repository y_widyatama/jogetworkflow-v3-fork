package telkom.isc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.joget.apps.app.model.AuditTrail;
import org.joget.plugin.base.AuditTrailPlugin;
import org.joget.plugin.base.DefaultPlugin;
import org.joget.plugin.base.PluginManager;
import org.joget.plugin.base.PluginProperty;
import org.joget.workflow.model.WorkflowActivity;
import org.joget.workflow.model.service.WorkflowManager;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.support.TransactionSynchronization;

import static java.lang.Class.*;

public class AuditPluginActivator extends DefaultPlugin  implements AuditTrailPlugin{

	private PluginManager pluginManager;
	private WorkflowManager workflowManager;
	private JdbcTemplate jdbcTemplate;
    private String jdbcurl,jdbcuser,jdbcpwd;
    private SimpleCache<String> completedProcesses = new SimpleCache<String>();
	public String getName() {
		return "Audit Trail plugin [ denormalizer]";
	}

	public String getVersion() {
		return "1.2";
	}
	JdbcTemplate getTemplate()
	{
		synchronized (this)
		{
		if (jdbcTemplate == null)
		{
            String jdbcConn = "jdbc:mysql://localhost:3307/jwdb";
            String jdbcUser = "root";
            String jdbcPass = "";
            if (jdbcurl !=null) jdbcConn = jdbcurl;
            if (jdbcuser != null) jdbcUser = jdbcuser;
            if (jdbcpwd != null) jdbcPass = jdbcpwd;
            try {
                if (jdbcConn.startsWith("jdbc:oracle")) forName("oracle.jdbc.OracleDriver");
                if (jdbcConn.startsWith("jdbc:mysql"))  forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            /*String jdbcConn = "jdbc:oracle:thin:@" + "10.65.10.171" + ":"
    		+ "1521" + ":" + "pdcto";
			String jdbcUser = "sppdtest";
			String jdbcPass = "sppdtest";*/
            
	//		jdbcPass = simpleDecrypt(jdbcPass);
			
			int maxConn = 20;
			GenericObjectPool pool = new GenericObjectPool();
			pool.setTestWhileIdle(true);
			pool.setTestOnBorrow(true);
			pool.setTimeBetweenEvictionRunsMillis(60*60*1000L);
			ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(jdbcConn, 
					jdbcUser, jdbcPass);
			PoolableConnectionFactory factory =  
			new PoolableConnectionFactory(connectionFactory,pool,null," SELECT 1 FROM DUAL ",false,true);
			pool.setMaxActive(maxConn);
			PoolingDataSource dataSource = new PoolingDataSource(pool);
			jdbcTemplate = new JdbcTemplate(dataSource);
			
		}
		}
		return jdbcTemplate;
	}
	public String getDescription() {
		return "Denormalize process data into tables";
	}

	public PluginProperty[] getPluginProperties() {
		PluginProperty[] properties = new PluginProperty[] { 
		        new PluginProperty("jdbcurl", "JDBC Url", PluginProperty.TYPE_TEXTFIELD, null, "jdbc:mysql://localhost:3307/jwdb"),
		        new PluginProperty("jdbcuser", "JDBC Username", PluginProperty.TYPE_TEXTFIELD, null, "root"),
		        new PluginProperty("jdbcpwd", "JDBC Password", PluginProperty.TYPE_TEXTFIELD, null, ""),
		    };
	return properties;
	}

	public Object execute(Map props) {
		System.err.println("telkom.isc.AuditPluginActivator::execute");
		AuditTrail currentTrail=null;
		for (Object key : props.keySet())
		{
			String type = "?";
			type = (props.get(key)).getClass().getCanonicalName();
			if (type==null) type = "<?>";
			//System.err.print("key "+key.toString()+" type "+type);
			if (props.get(key) instanceof String) {
                if ("jdbcurl".equals(key)) jdbcurl = (String)props.get(key);
                if ("jdbcpwd".equals(key)) jdbcpwd = (String)props.get(key);
                if ("jdbcuser".equals(key)) jdbcuser = (String)props.get(key);
                //System.err.println(props.get(key));
            }
			//else System.err.println();
			if (props.get(key) instanceof AuditTrail) currentTrail = (AuditTrail)props.get(key);
			if ((pluginManager==null) && (props.get(key) instanceof PluginManager)) pluginManager = (PluginManager)props.get(key);
		}
		if (currentTrail.getMethod().equals("assignmentComplete"))
		{
			String activityId = currentTrail.getMessage();
			if (workflowManager==null)
				workflowManager = (WorkflowManager) pluginManager.getBean("workflowManager");
			WorkflowActivity activity = workflowManager.getActivityById(activityId);
			if (activity == null)
			{
				return null;
			}
			String processId = activity.getProcessId();
			String userId = currentTrail.getUsername();
            if (completedProcesses.get(processId)!= null) return null; // already completed
			updateUserToLiveProcessMapping(userId,processId);
			insertInProgress(processId);
			
		}
		if (currentTrail.getMethod().equals("processCompleted"))
		{
			String processId = currentTrail.getMessage().intern();
			String userId = currentTrail.getUsername();
            completedProcesses.put(processId,processId,300); // 5 minutes
			//moveToCompleted(processId);
			// disable completion in auditplugin
			
		}
		return null;
	}

	private void moveToCompleted(String processId) {
		int year = 0;
		java.util.Date d = new java.util.Date();
		year =d.getYear() + 1900; // java.util.Date is really strange
		List<Object[]> list = getTemplate().query("SELECT USER_ID FROM YII_TOUCHED_BY_USER WHERE PROCESS_ID=?",
				new Object[] {processId},new GenericMapper());
		getTemplate().update("DELETE FROM YII_IN_PROGRESS WHERE PROCESS_ID = ? ",
				new Object[] { processId});
		for (Object[] r : list)
		{
			Object userId = r[0];
			if (userId!=null) {
				String userIdS= userId.toString();
				String lastCh = userIdS.substring(userIdS.length()-1);
                if ((lastCh.charAt(0)<'0') || (lastCh.charAt(0)>'9')) lastCh = "0";
				getTemplate().update("INSERT INTO YII_COMPLETED_" + year + "_N" + lastCh +
						" (USER_ID,PROCESS_ID)VALUES (?,?)",
						new Object[] {userIdS, processId});
			}
		}
	}

	private void insertInProgress(String processId) {
		List<Object[]> list = getTemplate().query("SELECT USER_ID FROM YII_TOUCHED_BY_USER WHERE PROCESS_ID=?",
				new Object[] {processId},new GenericMapper());
		List<Object[]> existing = getTemplate().query("SELECT USER_ID FROM YII_IN_PROGRESS WHERE PROCESS_ID=?",
				new Object[] {processId},new GenericMapper());
		HashMap<String, String> existingMap = new HashMap();
		for (Object[] r : existing)
		{
			existingMap.put(r[0].toString(), r[0].toString());
		}
			
		for (Object[] r : list)
		{
			Object userId = r[0];
			if (userId!=null) {
				String userIdS= userId.toString();
				if (existingMap.containsKey(userIdS))
				{
					continue;
				}
				getTemplate().update("INSERT INTO YII_IN_PROGRESS (USER_ID,PROCESS_ID)VALUES (?,?)",
						new Object[] {userId,processId});
				
			}
		}
	}

	private void updateUserToLiveProcessMapping(String userId, String processId) {
		List list = getTemplate().query("SELECT USER_ID,PROCESS_ID FROM YII_TOUCHED_BY_USER WHERE USER_ID=? AND PROCESS_ID=?",
				new Object[] {userId,processId},new GenericMapper());
		if (list.size()>0) return;
        TransactionSynchronization tx;
		getTemplate().update("INSERT INTO YII_TOUCHED_BY_USER (USER_ID,PROCESS_ID)VALUES (?,?)",new Object[] {userId,processId});
	}

	private static final class GenericMapper implements RowMapper {

	    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
	        Object[] rws;
	        rws = new Object[rs.getMetaData().getColumnCount()];
	        int i;
	        for (i=0; i<rws.length; i++)
	        	rws[i] = rs.getObject(i+1);
	        return rws;
	    }
	}

	
}